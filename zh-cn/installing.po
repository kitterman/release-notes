#
# Simplified Chinese translation of Lenny release notes
# Copyright (C) 2008 Debian Chinese project
# This file is distributed under the same license as the lenny release
# notes.
#
# Authors:
# R. N. Engch <niatlantice@gmail.com>, 2008.
# Deng Xiyue <manphiz@gmail.com>, 2008-2009.
# Dongsheng Song <dongsheng.song@gmail.com>, 2008-2009
# LI Daobing <lidaobing@gmail.com>, 2008.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <byang@debian.org>, 2019.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11.0\n"
"POT-Creation-Date: 2023-05-30 13:34+0800\n"
"PO-Revision-Date: 2023-05-30 13:35+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 3.3.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "zh_CN"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "安装系统"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  The methods that are available to "
"install your system depend on its architecture."
msgstr ""
"Debian 安装程序是 Debian 官方的安装系统。它提供了多种安装方式。实际可用的安装"
"方式取决于您的处理器架构。"

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"您可以在 <ulink url=\"&url-installer;\">Debian 网站</ulink>上找到 "
"&releasename; 的安装程序映像和安装手册。"

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"安装手册也可以在 Debian 官方 DVD（CD/蓝光光碟）的第一张盘上找到，路径是："

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>语言</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;"
"index#errata\">errata</ulink> for debian-installer for a list of known "
"issues."
msgstr ""
"您可能希望阅读<ulink url=\"&url-installer;index#errata\">勘误表</ulink>，以了"
"解 Debian 安装程序的已知问题列表。"

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "安装系统有哪些新特性？"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"自从 Debian 安装程序上一次随 &debian; &oldrelease; 发布以来，我们进行了大量的"
"开发工作，以提供更好的硬件支持及各种激动人心的新功能与改进。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"If you are interested in an overview of the changes since &oldreleasename;, "
"please check the release announcements for the &releasename; beta and RC "
"releases available from the Debian Installer's <ulink url=\"&url-installer-"
"news;\">news history</ulink>."
msgstr ""
"如果您想对从 &oldreleasename; 开始各项变更的情况有个大致的了解，请访问 "
"Debian 安装程序的<ulink url=\"&url-installer-news;\">新闻历史</ulink>并阅读 "
"&releasename; 的 beta 和 RC 版本的发布公告。"

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:52
msgid "Something"
msgstr "一些东西"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:54
msgid "Text"
msgstr "文本"

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:130
msgid "Cloud installations"
msgstr "云服务安装"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:132
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"&releasename; for several popular cloud computing services including:"
msgstr ""
"Debian <ulink url=\"&url-cloud-team;\">cloud team</ulink>（云团队）为部分流行"
"的云服务提供商直接提供 Debian &releasename; 的系统，包括："

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:139
msgid "Amazon Web Services"
msgstr "Amazon Web Services"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:144
msgid "Microsoft Azure"
msgstr "Microsoft Azure"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:149
msgid "OpenStack"
msgstr "OpenStack"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:154
msgid "Plain VM"
msgstr "普通 VM"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:161
msgid ""
"Cloud images provide automation hooks via <command>cloud-init</command> and "
"prioritize fast instance startup using specifically optimized kernel "
"packages and grub configurations.  Images supporting different architectures "
"are provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""
"云映像将使用 <command>cloud-init</command> 提供自动化的钩子，并使用特别优化过"
"的内核软件包和 grub 配置达到快速建立实例的效果。在需要的场合，映像将支持不同"
"的硬件架构；同时云团队致力于支持云服务提供商提供的所有功能。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:170
msgid ""
"The cloud team will provide updated images until the end of the LTS period "
"for &releasename;. New images are typically released for each point release "
"and after security fixes for critical packages.  The cloud team's full "
"support policy can be found <ulink url=\"&url-cloud-wiki-imagelifecycle;"
"\">here</ulink>."
msgstr ""
"云团队将提供映像更新，直至 &releasename; 的长期支持周期结束。新映像通常在每个"
"小版本更新发布时或者关键软件包的安全更新之后发布。云团队的完整支持策略可以在"
"<ulink url=\"&url-cloud-wiki-imagelifecycle;\">这里</ulink>找到。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:178
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""
"如需了解更多详情，请参考 <ulink url=\"&url-cloud;\">cloud.debian.org</ulink> "
"和<ulink url=\"&url-cloud-wiki;\">维基页面</ulink>。"

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:185
msgid "Container and Virtual Machine images"
msgstr "容器和虚拟机映像"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:187
msgid ""
"Multi-architecture Debian &releasename; container images are available on "
"<ulink url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the "
"standard images, a <quote>slim</quote> variant is available that reduces "
"disk usage."
msgstr ""
"可以在 <ulink url=\"&url-docker-hub;\">Docker Hub</ulink> 找到多种架构的 "
"Debian &releasename; 容器映像。除了标准映像之外，另有提供剪裁版"
"（<quote>slim</quote>版本）以便缩减磁盘使用量。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:193
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""
"用于 Hashicorp Vagrant VM manager 的虚拟机映像在 <ulink url=\"&url-vagrant-"
"cloud;\">Vagrant Cloud</ulink> 处发布。"

#~ msgid "Automated installation"
#~ msgstr "自动安装"

#~ msgid ""
#~ "Some changes in this release are not fully backwards-compatible with "
#~ "previous versions of preseeding, the process used for automatic "
#~ "installation."
#~ msgstr ""
#~ "本次发布版本的部分变更和之前版本的预置不能完全向后兼容。预置是自动安装所使"
#~ "用的进程。"

#~ msgid ""
#~ "If you have existing preseed configuration settings that worked with the "
#~ "&oldreleasename; installer, you should assume that modifications will be "
#~ "required for them to work correctly with the &releasename; installer."
#~ msgstr ""
#~ "如果您有 &oldreleasename; 安装程序能够正常使用的预置文件，您应当假定它们需"
#~ "要经过修改才能让 &releasename; 安装程序正常使用。"

#~ msgid ""
#~ "The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> "
#~ "contains an appendix with extensive documentation on preseeding."
#~ msgstr ""
#~ "<ulink url=\"&url-install-manual;\">安装手册</ulink>的附录中包含了如何使用"
#~ "预置文件的详细文档。"

#~ msgid ""
#~ "Some changes also imply changes in the support in the installer for "
#~ "automated installation using preconfiguration files.  This means that if "
#~ "you have existing preconfiguration files that worked with the "
#~ "&oldreleasename; installer, you cannot expect these to work with the new "
#~ "installer without modification."
#~ msgstr ""
#~ "一些改动意味着安装程序对使用预配置文件进行自动安装的支持将有所变化。这意味"
#~ "着您现有的可以在 &oldreleasename; 安装程序中正常使用的预配置文件不一定能在"
#~ "未经修改的情况下和新的安装程序搭配使用。"

#~ msgid "Help with installation of firmware"
#~ msgstr "帮助安装固件"

#~ msgid ""
#~ "More and more, peripheral devices require firmware to be loaded as part "
#~ "of the hardware initialization. To help deal with this problem, the "
#~ "installer has a new feature. If some of the installed hardware requires "
#~ "firmware files to be installed, the installer will try to add them to the "
#~ "system, based on a mapping from hardware ID to firmware file names."
#~ msgstr ""
#~ "越来越多的外设需要加载固件，作为硬件初始化过程的一部分。为了帮助解决此问"
#~ "题，安装程序有了一项新特性。如果某些已安装的硬件需要安装固件，安装程序将尝"
#~ "试将它们安装到系统中。这是基于从硬件标识符到固件文件名的映射实现的。"

#~ msgid ""
#~ "This new functionality is restricted to the unofficial installer images "
#~ "with firmware included (see <ulink url=\"&url-installer;"
#~ "#firmware_nonfree\">&url-installer;#firmware_nonfree</ulink>).  The "
#~ "firmware is usually not DFSG compliant, so it is not possible to "
#~ "distribute it in Debian's main repository."
#~ msgstr ""
#~ "这项新特性仅限于包含固件的非官方安装程序映像（参见 <ulink url=\"&url-"
#~ "installer;#firmware_nonfree\">&url-installer;#firmware_nonfree</ulink>）。"
#~ "这些固件通常不符合 Debian 自由软件指导方针（DFSG），所以无法在 Debian 主软"
#~ "件源中分发。"

#~ msgid ""
#~ "If you experience problems related to (missing) firmware, please read "
#~ "<ulink url=\"https://www.debian.org/releases/bullseye/amd64/"
#~ "ch06s04#completing-installed-system\">the dedicated chapter of the "
#~ "installation-guide</ulink>."
#~ msgstr ""
#~ "如果您遇到了和固件缺失有关的问题，请阅读<ulink url=\"https://www.debian."
#~ "org/releases/bullseye/amd64/ch06s04#completing-installed-system\">安装手册"
#~ "中的专门章节</ulink>。"
