Release Notes
-------------

The Release Notes contain important information for new users and for
an update from the previous version of Debian. It is written in
DocBook XML which allows the creation of various output files in the
formats HTML, text, PDF, ...

You can find more information about the Release Notes at
https://wiki.debian.org/ReleaseNotes
and
https://www.debian.org/doc/user-manuals#relnotes


Building the RN
---------------

To build it you need these packages:
 - docbook-xsl         (transformation stylesheets)
 - libxml2-utils       (for xmllint)
 - dblatex and xmlroff (depending on the output format and language)
 - xsltproc            (pdf generation)
 - po4a                (for handling PO file based translations)
 - make                (or you will type a lot)
 - fonts-freefont-ttf  (fonts)
 - fonts-sil-charis    (fonts)
 - fonts-arphic-*      (chinese fonts, while Chinese is currently disabled)
 - texlive-lang-all    (used by dblatex)
 - texlive-xetex       (used by dblatex)
 - w3m                 (to create the text version)
 - git                 (for creating translation statistics)

See also debian/control file.

Just call "make" to build it. To build the release notes in a single
format (html or txt or pdf) for a single language (e.g. zh_CN, en, es)
and the "all" pseudo-architecture, call e.g.:
"make html LINGUA=es architecture=all".

Contributing
------------

To submit new text to the Release Notes you can submit a merge request
against the repository at
https://salsa.debian.org/ddp-team/release-notes
or send a patch via the bts.

For example, to document an issue in the next Debian release, you
would add a `<section>` to the file 'en/issues.dbk'.

```xml
  <section id="description-of-issue">
   <title>A title describing the issue</title>

   <para>
     Introduce the issue, and explain briefly what the relevant
     packages do. This helps people reading the release notes decide
     whether they need to read this section or not. It may also
     bring the package to the attention of potential users.
   </para>

   <para>
    More details about the issue. Make sure to explain things
    simply, for non-experts. Users generally want to know what the
    issue is, how it affects them, and what they can do about
    it. This is particularly important when describing new features
    that may break existing system: how can they revert the
    change if it not suitable for them?
   </para>

   <para>
    Use as many paragraphs and sentences as needed,
    but try and keep things as short as possible.
   </para>

   <para>
    Keep the opening and closing 'para' tags on separate lines.
   </para>
  </section>
```

The release notes are written in DocBook (which is
described at https://tdg.docbook.org/) but it is recommended to keep
to a simple subset, as shown in the following examples:
  - To link to a website with more information:
   '`<ulink url="https://debian.org">Debian's home page<ulink>`'
  - To reference a man page (using manpages.debian.org):
   '`See <ulink url="&url-man;/&releasename;/systemd/journald.conf.5.html">journald.conf(5)</ulink>`'
  - To refer to a file or directory:
   '`See the comments in <filename>/etc/debian_version</filename> for more information`'
  - To refer to a command that the user will run:
    '`Run <programlisting>apt update</programlisting> to refresh the package lists`'.
		(Newlines inside a `<programlisting>` are preserved in the output).
  - To refer to a command as a noun:
    '`The <command>apt</command> program has new features`'
  - To refer to the name of a Debian package:
    '`The <systemitem role="package">apt</systemitem> package should be installed`'
  - For variables, or other things that don't quite fit into the above
    categories but need some markup: '`<literal>sid</literal>'
  - To mark text as important:
    '`It is very important to do this <emphasis>before</emphasis> rebooting`'
  - To put text in single quotes:
    '`The <quote>advanced package tool</quote> lets you manage packages`'

    This is particularly useful when you need to briefly introduce a technical
    phrase that new users may not be familiar with.
  - To refer to specific Debian code names, use lower case:
     '`trixie`'. You can also use
     '`&releasename;`' and '`&oldreleasename;`' for text that is likely to
    remain in the release notes for several releases.
	- (But the word 'Debian' itself should always be with an upper case
    'D' -- you can also use '`&debian;`' instead)
  - To make a list of bullet points:
```xml
  <para>The following things can be green:
    <itemizedlist>
      <listitem>grass</listitem>
      <listitem>houses</listitem>
    </itemizedlist>
  </para>
```
  (Generally `<listitem>`s are short with no full-stops at the end, but
  you can also put entire `<para>`s inside each `<listitem>`)


Translations
------------

Translators, please refer to the 'README.translators' document for
a full description of the translation process for this document.

Autobuilding of the RN
----------------------

The Release Notes are automatically built at www-master.debian.org in order to
publish them at https://www.debian.org/releases/stable/releasenotes,
https://www.debian.org/releases/testing/releasenotes and all the mirrors around
the world.

An automatic task updates the www-master GIT copy to the latest revision
and builds the Release Notes for different releases.

Build logs are available at
   https://www-master.debian.org/build-logs/webwml/release-notes.log


The automatic task that runs the build is:
https://salsa.debian.org/webmaster-team/cron/blob/master/parts/7release-notes
which is called by the 'often' task at www-master:

```
# m h               dom mon dow command
 24 3,7,11,15,19,23 *   *   *   /srv/www.debian.org/cron/often
```

CI Builds
---------

The release-notes are built on each commit on salsa.debian.org via a
gitlab pipeline.  The builds from the master branch will after a few
minutes be published on ddp-team.pages.debian.net.  Example links:

 * [English AMD64 version](https://ddp-team.pages.debian.net/release-notes/amd64/release-notes/index.en.html)
 * [English i386 version](https://ddp-team.pages.debian.net/release-notes/i386/release-notes/index.en.html)
 * [Translation statistics](https://ddp-team.pages.debian.net/release-notes/statistics.html)
